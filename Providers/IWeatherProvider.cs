using System.Collections.Generic;
using vuejs_lang.Models;

namespace vuejs_lang.Providers
{
    public interface IWeatherProvider
    {
        List<WeatherForecast> GetForecasts();
    }
}
